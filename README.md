# **CONYAR** - **CON**served **Y**ou **AR**e

CONYAR is a tool for easy identification of conserved regions in a protein.

## Version

1.0.0 (Last change: 21.09.22)

## Description

CONYAR was written in Python, but also uses R functions. It retrieves the amino acid sequences of a protein of interest from UniProtKB (https://www.uniprot.org/) and then performs a multiple sequence alignment (MSA) using Clustal Omega (http://clustal.org/omega/). After the MSA, conserved regions are extracted and the results are plotted using the R package ggmsa (http://yulab-smu.top/ggmsa/articles/ggmsa.html), which could be used by Rpy2 under Python. In a final report, the results are presented to the user in a compact form. In addition, the user has the possibility to further use the intermediate results in order to carry out more detailed analyses with other common tools. The results of an example analysis produced with CONYAR are available in the "Example" folder.

## Requirements

To use CONYAR, Python and R must be installed on the PC. Clustal Omega is also required. To run CONYAR, a Jupyter notebook is stored in the file "CONYAR.zip" and Clustal Omega is also stored in the folder "clustalo".

## License

Apache-2.0 (https://www.apache.org/licenses/LICENSE-2.0)

## Author

Aaron Babendreyer

### CONYAR is still under development
